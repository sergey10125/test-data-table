import React from 'react';
import DataTable from './DataTable';
import tableData from './tableData'

const columns = [
  { key: 'first_name', title: 'First name' },
  { key: 'last_name', title: 'Last name' },
  { key: 'email', title: 'Email' },
];

const App = ()  => (
  <div className="container">
    <DataTable columns={columns} data={tableData} />
  </div>
)

export default App;
