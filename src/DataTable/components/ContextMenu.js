import React, { useEffect, useRef, useCallback } from 'react'

const DropdownMenu = ({ isShow, actions, closeMenu, positions }) => {
  const contextMenuRef = useRef(null);

  const closeHeandle = useCallback(function closeHeandle(e) {
    if (!contextMenuRef.current.contains(e.target)) {
      document.removeEventListener('contextmenu', closeHeandle);
      document.removeEventListener('click', closeHeandle);
      closeMenu();
    }
  // eslint-disable-next-line
  }, [ closeMenu ]);

  const closeBeforeHendler = useCallback((func) => () => {
    func();
    closeMenu();
  }, [ closeMenu ]);


  useEffect(() => {
    if (isShow) {
      document.addEventListener('click', closeHeandle);
      document.addEventListener('contextmenu', closeHeandle);
    }
  }, [ isShow, closeHeandle ]);

  useEffect(() => {
    return () => {
      document.removeEventListener('contextmenu', closeHeandle);
      document.removeEventListener('click', closeHeandle);
    }
  // eslint-disable-next-line
  }, []);

  if (!isShow) return null;
  const style = { left: positions.x, top: positions.y };

  return (
    <div className='context-menu' style={style} ref={contextMenuRef}>
      {actions.map((action, index) => (
        <div
          key={index}
          className="context-menu__item"
          onClick={closeBeforeHendler(action.func)}
        >
          {action.title}
        </div>
      ))}
    </div>
  );
}


export default  DropdownMenu;
