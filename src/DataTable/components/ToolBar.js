import React, { useCallback } from 'react';
import * as actions from '../reducer/action/creators'

export default ({ state, dispatch }) => {
  const { options: { filterIsShow } } = state;

  const setFilterShowStatus = useCallback((status) => {
    dispatch(actions.setFilterShowStatus(status));
  }, [ dispatch ]);

  return (
    <ul className='table-toolbar' >
      <li
        className='table-toolbar__item'
        onClick={() => setFilterShowStatus(!filterIsShow)}
      >
        {filterIsShow
          ? 'Hide filter'
          : 'Show filter'
        }
      </li>
    </ul>
  )
}
