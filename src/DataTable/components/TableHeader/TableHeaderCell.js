import React, { useCallback } from 'react';

import * as actions from '../../reducer/action/creators'
import OrderView from './OrderView';


export default ({ data, dispatch, callContextMenu }) => {
  const { key, isShow, orderBy } = data;

  const setSortOrder = useCallback(() => {
    let patchOrderBy = 'asc';
    if (!!orderBy && orderBy === 'asc') {
      patchOrderBy = 'desc';
    };

    dispatch(actions.setOrder(key, patchOrderBy));
  }, [ key, orderBy, dispatch ]);


  if (!isShow) {
    return (
      <div
        className='data-table-cell data-table-cell--hide-header'
        onContextMenu={callContextMenu}
      >
      </div>
    )
  }

  return (
    <div
      className='data-table-cell data-table-cell--header'
      onClick={setSortOrder}
      onContextMenu={callContextMenu}
    >
      {data.title}
      <OrderView orderBy={data.orderBy} />
    </div>
  )
}
