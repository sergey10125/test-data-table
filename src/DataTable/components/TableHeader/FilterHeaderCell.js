import React, { useCallback } from 'react';
import * as actions from '../../reducer/action/creators'

export default ({ data, filters, dispatch }) => {
  const { isShow, key } = data;

  const changeFilterValue = useCallback((e, filters) => {
    const { value } = e.target;
    const newFilter = { ...filters };
    newFilter[key] = value;

    dispatch(actions.changeFilter(newFilter));
  }, [ key, dispatch ]);

  if (!isShow) {
    return (
      <div className='data-table-cell data-table-cell--hide'></div>
    )
  }

  return (
    <div
      className='data-table-cell data-table-cell--filter-cell'
    >
      <input
        className='data-table-input'
        onChange={(e) => changeFilterValue(e, filters)}
        value={filters[key] || ''}
        placeholder='Filter'
      />
    </div>
  )
}
