import React, { useCallback } from 'react';
import * as actions from '../../reducer/action/creators';

import TableHeaderCell from './TableHeaderCell';
import FilterHeaderCell from './FilterHeaderCell';


const TableHeader = ({ state, dispatch, callContextMenu }) => {
  const { columns, options: { filterIsShow, filters } } = state;

  const createContextActions = useCallback((index) => {
    const column = columns[index];
    if (!column.isShow) {
      return [{
        title: 'Show column',
        func: () => dispatch(actions.showColumn(column.key))
      }];
    } else {
      const actionsArray = [{
        title: 'Hide column',
        func: () => dispatch(actions.hideColumn(column.key))
      }];

      if (index !== 0) {
        actionsArray.push({
          title: 'Shift column to left',
          func: () => dispatch(actions.shiftColumnLeft(column.key))
        })
      }

      if (index !== (columns.length - 1)) {
        actionsArray.push({
          title: 'Shift column to right',
          func: () => dispatch(actions.shiftColumnRigth(column.key))
        })
      }

      return actionsArray;
    }
  }, [ columns, dispatch ])

  return (
    <div className='data-table__header'>
      <div className='data-table-row data-table-row--header'>
        {columns.map((column, index) => (
          <TableHeaderCell
            key={column.key}
            data={column}
            callContextMenu={callContextMenu(createContextActions(index))}
            dispatch={dispatch}
          />
        ))}
      </div>
      {filterIsShow && (
        <div className='data-table-row data-table-row--filter'>
          {columns.map(column => (
            <FilterHeaderCell
              key={column.key}
              data={column}
              filters={filters}
              dispatch={dispatch}
            />
          ))}
        </div>
      )}
    </div>
  );
}

export default TableHeader;
