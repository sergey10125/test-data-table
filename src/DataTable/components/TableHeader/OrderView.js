import React from 'react';

export default ({ orderBy }) => {
  let additionalClassName = '';
  if (orderBy === 'asc') {
    additionalClassName = 'order-view--asc';
  } else if (orderBy === 'desc') {
    additionalClassName = 'order-view--desc';
  }


  return (
    <div className={`order-view ${additionalClassName}`}></div>
  )
}
