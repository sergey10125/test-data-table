import React, { useState, useRef, useCallback, useMemo } from 'react';

import * as actions from '../../reducer/action/creators';

const indexSymbol = Symbol.for("index");

const TableBody = ({ row, columns, callContextMenu, dispatch }) => {
  const rowRef = useRef(null);
  const updateRowDataRef = useRef(null);
  const [ rowState, setRowState ] = useState({
    isEdit: false,
    data: row
  });

  updateRowDataRef.current = rowState.data;

  const outsideClick = useRef(function outsideClick(e) {
    if (!!rowRef.current && !rowRef.current.contains(e.target)) {
      document.removeEventListener('mousedown', outsideClick);

      const index = row[indexSymbol];
      dispatch(actions.editRow(index, updateRowDataRef.current));
      setRowState(rowState => ({ ...rowState, isEdit: false }));
    }
  }).current;

  const contextActions = useMemo(() => ([
    {
      title: 'Dublicate row',
      func: () => dispatch(actions.dublicateRow(row))
    },
    {
      title: 'Edit row',
      func: () => {
        document.addEventListener('mousedown', outsideClick);
        setRowState(rowState => ({ ...rowState, isEdit: true }))
      }
    },
    {
      title: 'Delete row',
      func: () => dispatch(actions.deleteRow(row[indexSymbol]))
    }
  ]), [ row, dispatch, outsideClick ])

  const changeRowState = useCallback((e, key) => {
    const { value } = e.target;
    setRowState( rowState => ({
      ...rowState,
      data: {
        ...rowState.data,
        [key]: value
      }
    }))
  }, [ setRowState ])




  return (
    <div
      className='data-table-row'
      onContextMenu={callContextMenu(contextActions)}
      ref={rowRef}
    >
      {columns.map(({ key, isShow }) => !isShow
        ? (<div key={key} className='data-table-cell data-table-cell--hide' />)
        : (
          <div key={key} className='data-table-cell'>
            {!rowState.isEdit
              ? row[key] || '-'
              : (
                <input
                  className='data-table-input'
                  value={rowState.data[key]}
                  onChange={(e) => changeRowState(e, key)}
                />
              )
            }
          </div>
        )
      )}
    </div>
  );
}

export default TableBody;
