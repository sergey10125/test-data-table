import React, { useRef, useEffect } from 'react';

import * as actions from '../../reducer/action/creators';
import formatedData from '../../utils/formatedData';

import scrollInButtom from '../../utils/scrollInButtom';
import throttle from '../../utils/throttle';
import debounce from '../../utils/debounce';

import TableRow from './TableRow'

const indexSymbol = Symbol.for("index");

const TableBody = ({ state, dispatch, callContextMenu }) => {
  const viewDataRef = useRef({});
  const tableBodyRef = useRef(null);

  const { columns, data, options: { filters, viewRowCount } } = state;
  const sortedColumn = columns.find(item => !!item.orderBy);


  const onTableInBottom = useRef(() => {
    const viewData = viewDataRef.current;
    if (
      !!viewData.viewRowCount
      && viewData.viewDataLength > viewData.viewRowCount
    ) {
      dispatch(actions.showMoreRow());
    }
  }).current;


  useEffect(() => {
    const scrollInButtomHandler = scrollInButtom(onTableInBottom);
    const scrollThrottle = throttle(scrollInButtomHandler, 100);
    const scrollDebounce = debounce(scrollInButtomHandler, 150);
    const tableBody = tableBodyRef.current;

    tableBody.addEventListener('scroll', scrollThrottle);
    tableBody.addEventListener('scroll', scrollDebounce);
    return () => {
      tableBody.removeEventListener('scroll', scrollThrottle);
      tableBody.removeEventListener('scroll', scrollDebounce);
    };
  }, [ onTableInBottom ])

  useEffect(() => {
    const tableBody = tableBodyRef.current;
    return () => {
      tableBody.scrollTop = 0;
    };
  }, [ sortedColumn, filters ])


  let viewData = formatedData(data, filters, sortedColumn);
  viewDataRef.current = { viewDataLength: viewData.length, viewRowCount };

  viewData = viewData.slice(0, viewRowCount);

  return (
    <div className='data-table__body' ref={tableBodyRef}>
      {viewData.map((row, index) => (
        <TableRow
          key={row[indexSymbol]}
          row={row}
          columns={columns}
          callContextMenu={callContextMenu}
          dispatch={dispatch}
        />
      ))}
    </div>
  );
}

export default TableBody;
