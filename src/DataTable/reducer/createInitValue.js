import normalizeColumnValues from '../utils/normalizeColumnValues';
import normalizeTableData from '../utils/normalizeTableData';

export default (columns, data) => {
  const normalizeColumns = normalizeColumnValues(columns);
  const normalizeData = normalizeTableData(data);
  return {
    columns: normalizeColumns,
    data: normalizeData || [],
    selectRows: [],
    options: {
      filterIsShow: false,
      filters: {},
      viewRowCount: 20
    }
  }
}
