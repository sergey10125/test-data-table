import * as types from './action/types'

const indexSymbol = Symbol.for("index");

const DEFAULT_ROW_COUNT = 20;
const ADDITIONAL_ROW_COUNT = 20;

const setFilterShowStatus = (state, payload) => {
  const { status } = payload;
  return {
    ...state,
    options: {
      ...state.options,
      filterIsShow: status
    }
  }
}

const setOrder = (state, payload) => {
  const { columnKey, sort } = payload;
  const { columns } = state;

  const newColumns = columns.map(column => {
    let orderBy = (column.key !== columnKey)? null : sort;
    return { ...column, orderBy };
  });

  return {
    ...state,
    columns: newColumns,
    options: {
      ...state.options,
      viewRowCount: DEFAULT_ROW_COUNT
    }
  }
};

const changeColumnVisibility = (state, payload, isShow) => {
  const { columnKey } = payload;
  const { columns } = state;

  const columnIndex = columns.findIndex(column => column.key === columnKey);
  if (columnIndex < 0) return state;

  const newColumns = [
    ...columns.slice(0, columnIndex),
    { ...columns[columnIndex], isShow },
    ...columns.slice(columnIndex + 1),
  ];

  return {
    ...state,
    columns: newColumns
  };
}

const shiftColumn = (state, payload, inRigth = true) => {
  const { columnKey } = payload;
  const newColumns = [ ...state.columns ]

  const index = newColumns.findIndex(column => column.key === columnKey);
  if (index < 0) return index;

  const removedColumn = newColumns.splice(index, 1).pop();
  const newIndex = index + ((inRigth)? 1 : -1);
  newColumns.splice(newIndex , 0, removedColumn);

  return {
    ...state,
    columns: newColumns
  }
}


const changeFilter = (state, payload) => ({
  ...state,
  options: {
    ...state.options,
    filters: payload,
    viewRowCount: DEFAULT_ROW_COUNT
  }
})

const dublicateRow = (state, payload) => {
  const newIndex = state.data.length;
  const newRow = { ...payload };
  newRow[indexSymbol] = newIndex;
  return {
    ...state,
    data: [
      ...state.data,
      newRow
    ]
  }
}

const editRow = (state, payload) => {
  const { index, row } = payload;
  const { data } = state;

  const newData = [
    ...data.slice(0, index),
    { ...data[index], ...row },
    ...data.slice(index + 1)
  ];

  return { ...state, data: newData };
}

const deleteRow = (state, payload) => {
  const { index } = payload;
  return {
    ...state,
    data: state.data.filter(row => row[indexSymbol] !== index)
  }
}

const showMoreRow = (state) => ({
  ...state,
  options: {
    ...state.options,
    viewRowCount: state.options.viewRowCount + ADDITIONAL_ROW_COUNT
  }
})





const reducer = (state, action) => {
  switch (action.type) {
    case types.SET_FILTER_SHOW_STATUS:
      return setFilterShowStatus(state, action.payload)
    case types.CHANGE_FILTER:
      return changeFilter(state, action.payload)

    case types.SET_ORDER:
      return setOrder(state, action.payload)
    case types.SHOW_COLUMN:
      return changeColumnVisibility(state, action.payload, true)
    case types.HIDE_COLUMN:
      return changeColumnVisibility(state, action.payload, false)
    case types.SHIFT_COLUMN_RIGTH:
      return shiftColumn(state, action.payload, true)
    case types.SHIFT_COLUMN_LEFT:
      return shiftColumn(state, action.payload, false)

    case types.DUBLICATE_ROW:
      return dublicateRow(state, action.payload)
    case types.EDIT_ROW:
      return editRow(state, action.payload)
    case types.DELETE_ROW:
      return deleteRow(state, action.payload)
    case types.SHOW_MORE_ROW:
      return showMoreRow(state, action.payload)
    default:
      return state
  }
}

export default reducer;
