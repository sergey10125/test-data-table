import * as types from './types'

export const setFilterShowStatus = (status) => ({
  type: types.SET_FILTER_SHOW_STATUS,
  payload: { status }
})

export const setOrder = (columnKey, sort = 'asc') => ({
  type: types.SET_ORDER,
  payload: { columnKey, sort }
})

export const hideColumn = (columnKey) => ({
  type: types.HIDE_COLUMN,
  payload: { columnKey }
})

export const showColumn = (columnKey) => ({
  type: types.SHOW_COLUMN,
  payload: { columnKey }
})

export const shiftColumnRigth = (columnKey) => ({
  type: types.SHIFT_COLUMN_RIGTH,
  payload: { columnKey }
})

export const shiftColumnLeft = (columnKey) => ({
  type: types.SHIFT_COLUMN_LEFT,
  payload: { columnKey }
})

export const changeFilter = (filterOptions) => ({
  type: types.CHANGE_FILTER,
  payload: filterOptions
})

export const showMoreRow = () => ({
  type: types.SHOW_MORE_ROW
})

export const dublicateRow = (row) => ({
  type: types.DUBLICATE_ROW,
  payload: row
})

export const editRow = (index, row) => ({
  type: types.EDIT_ROW,
  payload: { index, row }
})

export const deleteRow = (index) => ({
  type: types.DELETE_ROW,
  payload: { index }
})

export const editRows = (editRows) => ({
  type: types.CHANGE_FILTER,
  payload: editRows
})
