const normalizeItemFunction = (column) => {
  if (typeof column === 'string') {
    if (!column) {
      throw new Error('DataTable error: String item from array \'columns\' cannot by empty')
    }
    return { key: column, title: column, isShow: true, orderBy: null }
  }

  if (typeof column === 'object') {
    if (typeof column.key === 'undefined') {
      throw new Error('DataTable error: Expect \'key\' inside column object element from array \'columns\'')
    }

    return {
      key: column.key,
      title: column.title || column.key,
      isShow: true,
      orderBy: null
    }
  } else {
    throw new Error('DataTable error: Unexpected element format from array \'columns\'')
  }
}

export default (columns, data) => {
  const normalizeColumns = columns.map(column => normalizeItemFunction(column));

  // Detect duplicates keys
  const keyArray = normalizeColumns.map(item => item.key);
  if (keyArray.length !== Array.from(new Set(keyArray)).length) {
    throw new Error('DataTable error: Duplicates key inside \'columns\' array');
  }

  return normalizeColumns;
}
