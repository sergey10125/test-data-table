

export default (func, offsetFromBottom = 100) => (e) => {
  const elem = e.target;
  if (
    (elem.scrollTop + offsetFromBottom)
    >= (elem.scrollHeight - elem.offsetHeight)
  ) {
    func();
  }
};
