import React, { useState, useCallback } from 'react';
import ContextMenu from '../components/ContextMenu';

const DEFAULT_STATE = {
  isShow: false,
  actions: [],
  positions: { x: 0, y: 0 }
}

const isAbsoluteBased = (el) => {
  const { position } = getComputedStyle(el);
  return ['fixed', 'relative', 'absolute'].includes(position);
}
const findParentForAbsoluteEl = (el) => {
  let parentEl = el.parentElement;
  while (!isAbsoluteBased(parentEl)) {
    parentEl = parentEl.parentElement;
  };
  return parentEl;
}



const useContextMenu = () => {
  const [ state, setState ] = useState(DEFAULT_STATE);
  const closeMenu = useCallback(() => setState(DEFAULT_STATE), [ setState ]);

  const ContextMenuComponent = () => (
    <ContextMenu
      isShow={state.isShow}
      actions={state.actions}
      positions={state.positions}
      closeMenu={closeMenu}
    />
  );


  return {
    ContextMenu: ContextMenuComponent,
    callContextMenu: (actions) => function (e) {
      const BaseEl = findParentForAbsoluteEl(e.target)
      const offset = BaseEl.getBoundingClientRect();
      const x = Math.round(e.clientX - offset.x);
      const y = Math.round(e.clientY - offset.y + BaseEl.scrollTop);

      e.preventDefault();
      setState({
        isShow: true,
        actions,
        positions: { x, y }
      });
    }
  }
}

export default useContextMenu;
