export default (data, filters, sortedColumn) => {
  let viewData = data.filter(row => {
    return Object.keys(filters).every(key => {
      const rowValue = String(row[key]).toLowerCase();
      const filterValue = filters[key].toLowerCase();
      return rowValue.indexOf(filterValue) === 0;
    })
  })

  if (sortedColumn) {
    viewData = viewData.sort((a, b) => {
      const aString = String(a[sortedColumn.key]).toLowerCase();
      const bString = String(b[sortedColumn.key]).toLowerCase();

      let comparison = 0;
      if (aString > bString) {
        comparison = 1;
      } else if (aString < bString) {
        comparison = -1;
      }

      return (
        (sortedColumn.orderBy === 'asc') ? (comparison * -1) : comparison
      );
    })
  }

  return viewData;
}
