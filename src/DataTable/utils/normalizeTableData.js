const indexSymbol = Symbol.for("index");

export default (data) => data.map((item, index) => {
  const normalizeIteam = { ...item };
  normalizeIteam[indexSymbol] = index;
  return normalizeIteam;
});
