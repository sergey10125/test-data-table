import React, { useReducer, useMemo } from 'react';
import PropTypes from 'prop-types';

import useContextMenu from './utils/useContextMenu';
import createInitValue from './reducer/createInitValue';
import reducer from './reducer/reducerHandler';

import DataTableHeader from './components/TableHeader'
import DataTableBody from './components/TableBody'
import DataToolBar from './components/ToolBar'

import './index.css'

const propTypes = {
  columns: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.shape({
        key: PropTypes.string.isRequired,
        title: PropTypes.string,
      }),
    ]),
  ),
  data: PropTypes.arrayOf(PropTypes.object)
}


const DataTable = ({ columns, data }) => {
  const initValue = useMemo(
    () => createInitValue(columns, data),
    [ columns, data ]
  )

  const { ContextMenu, callContextMenu } = useContextMenu();
  const [state, dispatch] = useReducer(reducer, initValue);

  return (
    <div className='data-table-contaier'>
      <DataToolBar state={state} dispatch={dispatch} />
      <div className='data-table'>
        <DataTableHeader
          state={state}
          dispatch={dispatch}
          callContextMenu={callContextMenu}
        />
        <DataTableBody
          state={state}
          dispatch={dispatch}
          callContextMenu={callContextMenu}
        />
      </div>
      <ContextMenu/>
    </div>
  )
}

DataTable.propTypes = propTypes;
export default DataTable;
